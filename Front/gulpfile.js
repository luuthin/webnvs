const { src, dest, series } = require('gulp');
const htmlPartial = require('gulp-html-partial');
const concat = require('gulp-concat');
const minifyCSS = require('gulp-minify-css');
const autoprefixer = require('gulp-autoprefixer');

function pack_css(){
    return src('src/css/*.css')
    .pipe(minifyCSS())
    .pipe(autoprefixer('last 2 version', 'safari 5', 'ie 8', 'ie 9'))
    .pipe(concat('style.min.css'))
    .pipe(dest('build/css'));
}

function pack_html(){
    return src('src/*.html')
    .pipe(htmlPartial({
        basePath: 'src/partial/'
    }))
    .pipe(dest('build'));
}
 
exports.default = series(pack_css, pack_html);