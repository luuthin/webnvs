//Layout
$(function() {
    'use strict';
    
    var $isAnimatedFirst = $('.row-1 .is-animated'),
        $isAnimatedSecond = $('.row-2 .is-animated'),
        $isAnimatedThird = $('.row-3 .is-animated'),
        $isAnimatedFourth = $('.row-4 .is-animated'),
        $isAnimatedSixth = $('.row-6 .is-animated'),
        $isAnimatedSeventh = $('.row-7 .is-animated');

    $('#fullpage').fullpage({
        'controlArrows': false,
        'navigation': true,
        'scrollOverflow': true,
        'anchors': ['sec1', 'sec2', 'sec3', 'sec4', 'sec5', 'sec6', 'sec7'],
        onLeave: function(index, nextIndex, direction) {
            if ((index == 7 || index == 6 || index == 5 || index == 4 || index == 3 || index == 2) && nextIndex == 1) {
                $isAnimatedFirst.eq(0).addClass('animated fadeInLeft').css('animation-delay', '.3s');
                $isAnimatedFirst.eq(1).addClass('animated fadeInLeft').css('animation-delay', '.6s');
                $isAnimatedFirst.eq(2).addClass('animated fadeInLeft').css('animation-delay', '.9s');
                $isAnimatedFirst.eq(3).addClass('animated fadeInLeft').css('animation-delay', '1.2s');
                $isAnimatedFirst.eq(4).addClass('animated fadeInUp').css('animation-delay', '1.5s');
            }
            else if ((index == 1 && nextIndex == 2) || ((index == 7 || index == 6 || index == 5 || index == 4 || index == 3) && nextIndex == 2)) {
                $isAnimatedSecond.eq(0).addClass('animated fadeInLeft').css('animation-delay', '.3s');
                $isAnimatedSecond.eq(1).addClass('animated fadeInRight').css('animation-delay', '.6s');
                $isAnimatedSecond.eq(2).addClass('animated fadeInUp').css('animation-delay', '.8s');
                $isAnimatedSecond.eq(3).addClass('animated fadeInUp').css('animation-delay', '.95s');
                $isAnimatedSecond.eq(4).addClass('animated fadeInUp').css('animation-delay', '1.1s');
                $isAnimatedSecond.eq(5).addClass('animated fadeInUp').css('animation-delay', '1.25s');
                $isAnimatedSecond.eq(6).addClass('animated fadeInUp').css('animation-delay', '1.4s');
                $isAnimatedSecond.eq(7).addClass('animated fadeInUp').css('animation-delay', '1.55s');
            }
            else if (((index == 1 || index == 2) && nextIndex == 3) || ((index == 7 || index == 6 || index == 5 || index == 4) && nextIndex == 3)) {
                $isAnimatedThird.eq(0).addClass('animated fadeInLeft').css('animation-delay', '.3s');
                $isAnimatedThird.eq(1).addClass('animated fadeInDown').css('animation-delay', '.6s');
                $isAnimatedThird.eq(2).addClass('animated fadeInUp').css('animation-delay', '.9s');
            }
            else if (((index == 1 || index == 2 || index == 3) && nextIndex == 4) || ((index == 7 || index == 6 || index == 5) && nextIndex == 4)) {
                $isAnimatedFourth.eq(0).addClass('animated fadeInLeft').css('animation-delay', '.3s');
                $isAnimatedFourth.eq(1).addClass('animated fadeInDown').css('animation-delay', '.6s');
                $isAnimatedFourth.eq(2).addClass('animated fadeInRight').css('animation-delay', '.6s');
                $isAnimatedFourth.eq(3).addClass('animated fadeInRight').css('animation-delay', '.6s');
                $isAnimatedFourth.eq(4).addClass('animated fadeInUp').css('animation-delay', '.6s');
            }
            else if (((index == 1 || index == 2 || index == 3 || index == 4 || index == 5) && nextIndex == 6) || (index == 7 && nextIndex == 6)) {
                $isAnimatedSixth.eq(0).addClass('animated fadeInUp').css('animation-delay', '.3s');
                $isAnimatedSixth.eq(1).addClass('animated fadeInUp').css('animation-delay', '.6s');
                $isAnimatedSixth.eq(2).addClass('animated fadeInUp').css('animation-delay', '.9s');
            }
            else if ((index == 1 || index == 2 || index == 3 || index == 4 || index == 5 || index == 6) && nextIndex == 7) {
                $isAnimatedSeventh.eq(0).addClass('animated fadeInDown').css('animation-delay', '.3s');
                $isAnimatedSeventh.eq(1).addClass('animated fadeInDown').css('animation-delay', '.3s');
                $isAnimatedSeventh.eq(2).addClass('animated fadeInUp').css('animation-delay', '.3s');
                $isAnimatedSeventh.eq(3).addClass('animated fadeInDown').css('animation-delay', '.3s');
                $isAnimatedSeventh.eq(4).addClass('animated fadeInUp').css('animation-delay', '.3s');
            }

            if (index === 1) {
                $isAnimatedFirst.eq(0).removeClass('animated fadeInLeft');
                $isAnimatedFirst.eq(1).removeClass('animated fadeInLeft');
                $isAnimatedFirst.eq(2).removeClass('animated fadeInLeft');
                $isAnimatedFirst.eq(3).removeClass('animated fadeInLeft');
                $isAnimatedFirst.eq(4).removeClass('animated fadeInUp');
            }
            else if (index === 2) {
                $isAnimatedSecond.eq(0).removeClass('animated fadeInLeft');
                $isAnimatedSecond.eq(1).removeClass('animated fadeInRight');
                $isAnimatedSecond.eq(2).removeClass('animated fadeInUp');
                $isAnimatedSecond.eq(3).removeClass('animated fadeInUp');
                $isAnimatedSecond.eq(4).removeClass('animated fadeInUp');
                $isAnimatedSecond.eq(5).removeClass('animated fadeInUp');
                $isAnimatedSecond.eq(6).removeClass('animated fadeInUp');
                $isAnimatedSecond.eq(7).removeClass('animated fadeInUp');
            }
            else if (index === 3) {
                $isAnimatedThird.eq(0).removeClass('animated fadeInLeft');
                $isAnimatedThird.eq(1).removeClass('animated fadeInDown');
                $isAnimatedThird.eq(2).removeClass('animated fadeInUp');
            }
            else if (index === 4) {
                $isAnimatedFourth.eq(0).removeClass('animated fadeInLeft');
                $isAnimatedFourth.eq(1).removeClass('animated fadeInDown');
                $isAnimatedFourth.eq(2).removeClass('animated fadeInRight');
                $isAnimatedFourth.eq(3).removeClass('animated fadeInRight');
                $isAnimatedFourth.eq(4).removeClass('animated fadeInUp');
            }
            else if (index === 6) {
                $isAnimatedSixth.eq(0).removeClass('animated fadeInUp');
                $isAnimatedSixth.eq(1).removeClass('animated fadeInUp');
                $isAnimatedSixth.eq(2).removeClass('animated fadeInUp');
            }
            else if (index === 7) {
                $isAnimatedSeventh.eq(0).removeClass('animated fadeInDown');
                $isAnimatedSeventh.eq(1).removeClass('animated fadeInDown');
                $isAnimatedSeventh.eq(2).removeClass('animated fadeInUp');
                $isAnimatedSeventh.eq(3).removeClass('animated fadeInDown');
                $isAnimatedSeventh.eq(4).removeClass('animated fadeInUp');
            }
        }
    });
});


//Nav Button
$('.toggle-nav').on('click', function() {
    $(this).parent().next().addClass('full');
    $('body').addClass('overflow-menu');
});
$('.closebtn').on('click', function(e) {
    e.preventDefault();
    $(this).parentsUntil('.nav-wrapper').removeClass('full');
    $('body').removeClass('overflow-menu');
});


//Slide Tech Icons
$('.icons-wrapper .slider').slick({
    autoplay: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    arrows: false,
    fade: false,
    responsive: [{
        breakpoint: 767,
        settings: {
            slidesToShow: 1
        }
    }]
});


//Show-Hide Communities
$('.row-5 .sidebar a').on('click', function(e) {
    e.preventDefault();
    var target = $($(this).attr('href'));
    $('.row-5 .mBar > div').removeClass('active');
    $(this).parent().addClass('active');
    $('.row-5 .content > div').addClass('animated fadeOutUp');
    $('.row-5 .content > div').removeClass('show animated fadeInUp fadeOutUp');
    target.addClass('animated fadeInUp show');
});


//Album Home
$('.images-wrapper .slider').slick({
    autoplay: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: false
});

$('[data-fancybox]').fancybox({
    loop: true,
    buttons: [
        "zoom",
        "slideShow",
        "fullScreen",
        "download",
        "thumbs",
        "close"
    ],
    protect: true,
    thumbs : {
        autoStart: true,
        axis: 'x'
    }
});


//Show-Hide Products
$('.products-wrapper .products-list a').on('click', function(e) {
    e.preventDefault();
    var target = $($(this).attr('href'));
    $('.products-wrapper .products-list .box').removeClass('active');
    $(this).parent().addClass('active');
    $('.products-wrapper .products-content > div').addClass('animated fadeOut');
    $('.products-wrapper .products-content > div').removeClass('show animated fadeIn fadeOut');
    target.addClass('animated fadeIn show');
});

$("#products-list").change(function(){
    $('.products-wrapper .products-content > div').addClass('animated fadeOut');
    $('.products-wrapper .products-content > div').removeClass('show animated fadeIn fadeOut');
    $(".products-wrapper #product-" + this.value).addClass('animated fadeIn show');
    console.log(this.value);
});
$("#products-list").change();


//Sync Tabs on Products Page
$('.row-2 a').on('click', function(e) {
    e.preventDefault();
    var redirect = $(this).attr('href');
    window.location.href = "products.html" + redirect;
    console.log(redirect);
});

$('#myNav .sync-tab a').on('click', function(e) {
    e.preventDefault();
    var redirect = $(this).attr('href');
    window.location.href = "https://mobilink.vn/web/fds/san-pham" + redirect;
});

$(document).ready(function () {
    var hash = window.location.hash;
    history.replaceState(null, null, ' ');
    $(hash).children().click();
});


//Album Products - OpenCPS
$('.products-samples .slider').slick({
    autoplay: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    arrows: false,
    fade: false,
    responsive: [{
        breakpoint: 767,
        settings: {
            slidesToShow: 1,
            arrows: true
        }
    }]
});


//Small Scroll
$('.row-5 .description').attr('data-simplebar', '');