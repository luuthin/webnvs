const { src, dest, series, parallel, watch } = require('gulp');
const htmlPartial = require('gulp-html-partial');
const concat = require('gulp-concat');
const minifyCSS = require('gulp-minify-css');
const autoprefixer = require('gulp-autoprefixer');
const htmlmin = require('gulp-htmlmin');
const browsersync = require("browser-sync").create();
const sass = require('gulp-sass');

// BrowserSync
function browserSync(done) {
    browsersync.init({
        server: {
            baseDir: "./build/"
        },
        port: 3000
    });
    done();
}

// BrowserSync Reload
function browserSyncReload(done) {
    browsersync.reload();
    done();
}

// Minify CSS
function pack_css() {
    return src('src/css/*.css')
        .pipe(minifyCSS())
        .pipe(autoprefixer('last 2 version', 'safari 5', 'ie 8', 'ie 9'))
        .pipe(concat('style.min.css'))
        .pipe(dest('build/css'));
}

// Copy js
function pack_js() {
    return src('src/js/*.js')
        .pipe(dest('build/js'))
}

// Include html
function pack_html() {
    return src('src/*.html')
        .pipe(htmlPartial({
            basePath: 'src/partial/'
        }))
        // .pipe(htmlmin({
        //     collapseWhitespace: true,
        //     removeComments: true
        //   }))
        .pipe(dest('build'));
}

// Watch change file and reload
function watchFiles() {
    return watch(
        [
            "src/css/*.css",
            'src/**/*.html',
            'src/*.html',
            "src/js/*.js"
        ], series(pack_html, pack_css, pack_js, browserSyncReload));
}

exports.pack_css = series(pack_css);
exports.pack_html = series(pack_html);
exports.pack_js = series(pack_js);

exports.default = parallel(pack_html, pack_css, pack_js, browserSync, watchFiles);