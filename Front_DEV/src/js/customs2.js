var is_open_menu = false;

//comment

$(document).ready(function () {

    //toggle menu
    $('.toggle-nav').click(function () {
        toggle_menu()
    });

    // close menu khi chọn page
    $('.item-menu-click').click(function () {
        
        toggle_menu();
    })

    $('.owl-carousel-customer').owlCarousel({
        items: 1,
        loop: true,
        autoplay: false,
        dots: true,
        nav: true
    }, 3000);

    $('.post-content').owlCarousel({
        items: 1,
        loop: true,
        autoplay: false,
        dots: false,
        nav: false
    }, 3000);

    $(".products").imagesGrid({
        rowHeight: 250,
        margin: 0,
        imageSelector: '.product-image'
    });

    initCanvasTag();
});

function toggle_menu() {
    is_open_menu = !is_open_menu;
    $('.nav-open').toggleClass('active');
    $('.nav-close').toggleClass('active');

    if (is_open_menu) {
        $('.dropdown-nav').toggleClass('full');
        $('.home-theme').toggleClass('overflow-menu');
        $("#header label span:nth-child(2)").css("color","#000");
        $(".nav-close").css("color","#fff");
    }else{
        $('.dropdown-nav').removeClass('full');
        $("#header label span:nth-child(2)").css("color","#000");
        $(".nav-close").css("color","#000");
    }
}

function initCanvasTag(){
    $('#myCanvas').tagcanvas({
        textColour : '#000',
        outlineColour : '#fff',
        outlineThickness : 0,
        maxSpeed : 0.1,
        depth : 0.75,
        shape: "sphere"
    }, 'canvas-tags')
}