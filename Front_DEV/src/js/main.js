//Layout
$(function() {
    'use strict';
    $('#page2').hide();
    $('.row-5-page2').hide();

    $('#see_more').click(function () {
        window.location.href = "gioithieu_2.html";
        // $('#page2').show()
        // $('#page2').addClass('animated is-animated fadeInRight');
        // $('#page1').hide()
    });

    $('#product-detail-item').click(function () {
        $('#product-detail').show()
        $('#product-detail').addClass('animated is-animated fadeInLeft');
        $('#page-product').hide();
    });

    $('#come_back').click(function () {
        $('#page1').show()
        $('#page1').addClass('animated is-animated fadeInRight');
        $('#page2').hide()
        $('#page-product').show()
        $('#page-product').addClass('animated is-animated fadeInRight');
        $('#product-detail').hide()
    });

    $('.next').click(function() {
        $('.row-5-page2').show();
        $('.row-5-page2').addClass('animated is-animated fadeInLeft');
        $('.row-5-page1').hide();
        $('.next').attr("src","images/icon/NR-12.png");
        $('.next').css({"transform": "rotate(180deg)"});
        $('.prev').attr("src","images/icon/N&R-11.png");
        $('.prev').css({"transform": "rotate(180deg)"});
    })

    $('.prev').click(function() {
        $('.row-5-page1').show();
        $('.row-5-page1').addClass('animated is-animated fadeInLeft');
        $('.row-5-page2').hide();
        $('.next').attr("src","images/icon/N&R-11.png");
        $('.next').css({"transform": "rotate(0)"});
        $('.prev').attr("src","images/icon/NR-12.png");
        $('.prev').css({"transform": "rotate(0)"});
    })

    $('.next_gt').click(function() {
        $('#page1 .responsive').addClass('animated fadeInLeft');
        $('#page1 .responsive').attr("src","images/icon/website otp 3-47.png");
        $('#page1 .summer_class').html('SUMMER 2018')
        $('.next_gt').attr("src","images/icon/NR-12.png");
        $('.next_gt').css({"transform": "rotate(180deg)"});
        $('.prev_gt').attr("src","images/icon/N&R-11.png");
        $('.prev_gt').css({"transform": "rotate(180deg)"});
    })

    $('.prev_gt').click(function() {
        $('#page1 .responsive').removeClass('animated fadeInLeft');
        $('#page1 .responsive').addClass('animated is-animated fadeInLeft');
        
        $('#page1 .responsive').attr("src","images/icon/website otp 3-26.png");
        $('#page1 .summer_class').html('SUMMER 2017')
        $('.next_gt').attr("src","images/icon/N&R-11.png");
        $('.next_gt').css({"transform": "rotate(0)"});
        $('.prev_gt').attr("src","images/icon/NR-12.png");
        $('.prev_gt').css({"transform": "rotate(0)"});
    })

    var $isAnimatedFirst = $('.row-1 .is-animated'),
        $isAnimatedSecond = $('.row-2 .is-animated'),
        $isAnimatedThird = $('.row-3 .is-animated'),
        $isAnimatedFourth = $('.row-4 .is-animated'),
        $isAnimatedFive = $('.row-4 .is-animated'),
        $isAnimatedSixth = $('.row-6 .is-animated'),
        $isAnimatedSeventh = $('.row-7 .is-animated');

    $('#fullpage').fullpage({
        'controlArrows': false,
        'navigation': true,
        'scrollOverflow': true,
        'anchors': ['sec1', 'sec2', 'sec3', 'sec4', 'sec5', 'sec6', 'sec7'],
        onLeave: function(index, nextIndex, direction) {
            // custom style button menu
            if (nextIndex != 1) {
                $('.nav-open').css('color', '#333');
            } else {
                $('.nav-open').css('color', '#fff');
            }

            if ((index == 7 || index == 6 || index == 5 || index == 4 || index == 3 || index == 2) && nextIndex == 1) {
                $isAnimatedFirst.eq(0).addClass('animated fadeInLeft').css('animation-delay', '.3s');
                $(".logo_class").attr("src","images/icon/lohg 2-48.png");
                $("#header label span:nth-child(2)").css("color","#fff");
                $("#header").removeClass('header');
            }
            else if ((index == 1 && nextIndex == 2) || ((index == 7 || index == 6 || index == 5 || index == 4 || index == 3) && nextIndex == 2)) {
                $(".logo_class").attr("src","images/icon/logo-10.png");
                $("#header label span:nth-child(2)").css("color","black");
                $("#header").removeClass('header');
            }
            else if (((index == 1 || index == 2) && nextIndex == 3) || ((index == 7 || index == 6 || index == 5 || index == 4) && nextIndex == 3)) {
                $('.img-back').show();
                $(".logo_class").attr("src","images/icon/logo-10.png");
                $("#header label span:nth-child(2)").css("color","black");
                $("#header").removeClass('header');
            }
            else if (((index == 1 || index == 2 || index == 3) && nextIndex == 4) || ((index == 7 || index == 6 || index == 5) && nextIndex == 4)) {
                $(".logo_class").attr("src","images/icon/logo-10.png");
                $("#header label span:nth-child(2)").css("color","black");
                $("#header").removeClass('header');
            }
            else if (((index == 1 || index == 2 || index == 3 || index == 4) && nextIndex == 5) || ((index == 7 || index == 6) && nextIndex == 5)) {
                $isAnimatedSixth.eq(0).addClass('animated fadeInDown').css('animation-delay', '.3s');
                $isAnimatedSixth.eq(1).addClass('animated fadeInDown').css('animation-delay', '.6s');
                $isAnimatedSixth.eq(2).addClass('animated fadeInDown').css('animation-delay', '.9s');
                $(".logo_class").attr("src","images/icon/logo-10.png");
                $("#header label span:nth-child(2)").css("color","black");
                $("#header").addClass('header');
            }
            else if (((index == 1 || index == 2 || index == 3 || index == 4 || index == 5) && nextIndex == 6) || (index == 7 && nextIndex == 6)) {
                $('.contact_class').show();
                $('.row-6 .contact').addClass('contact_class');
                $isAnimatedSixth.eq(0).addClass('animated fadeInDown').css('animation-delay', '.3s');
                $isAnimatedSixth.eq(1).addClass('animated fadeInDown').css('animation-delay', '.6s');
                $isAnimatedSixth.eq(2).addClass('animated fadeInDown').css('animation-delay', '.9s');
                $(".logo_class").attr("src","images/icon/logo-10.png");
                $("#header label span:nth-child(2)").css("color","black");
                $("#header").addClass('header');
            }

            if (index === 1) {
                $isAnimatedFirst.eq(0).removeClass('animated fadeInLeft');
                $isAnimatedFirst.eq(1).removeClass('animated fadeInLeft');
                $isAnimatedFirst.eq(2).removeClass('animated fadeInLeft');
                $isAnimatedFirst.eq(3).removeClass('animated fadeInLeft');
                $isAnimatedFirst.eq(4).removeClass('animated fadeInUp');
            }
            else if (index === 2) {
                $isAnimatedSecond.eq(0).removeClass('animated fadeInLeft');
                $isAnimatedSecond.eq(1).removeClass('animated fadeInRight');
                $isAnimatedSecond.eq(2).removeClass('animated fadeInUp');
                $isAnimatedSecond.eq(3).removeClass('animated fadeInUp');
                $isAnimatedSecond.eq(4).removeClass('animated fadeInUp');
                $isAnimatedSecond.eq(5).removeClass('animated fadeInUp');
                $isAnimatedSecond.eq(6).removeClass('animated fadeInUp');
                $isAnimatedSecond.eq(7).removeClass('animated fadeInUp');
            }
            else if (index === 3) {
                $isAnimatedThird.eq(0).removeClass('animated fadeInLeft');
                $isAnimatedThird.eq(1).removeClass('animated fadeInDown');
                $isAnimatedThird.eq(2).removeClass('animated fadeInUp');
            }
            else if (index === 4) {
                $isAnimatedFourth.eq(0).removeClass('animated fadeInLeft');
                $isAnimatedFourth.eq(1).removeClass('animated fadeInDown');
                $isAnimatedFourth.eq(2).removeClass('animated fadeInRight');
                $isAnimatedFourth.eq(3).removeClass('animated fadeInRight');
                $isAnimatedFourth.eq(4).removeClass('animated fadeInUp');
            }
            else if (index === 6) {
                $isAnimatedSixth.eq(0).removeClass('animated fadeInDown');
            }
        }
    });
});


//Nav Button
$('.toggle-nav').on('click', function() {
    $(this).parent().next().addClass('full');
    $('body').addClass('overflow-menu');
});
$('.closebtn').on('click', function(e) {
    e.preventDefault();
    $(this).parentsUntil('.nav-wrapper').removeClass('full');
    $('body').removeClass('overflow-menu');
});



//Show-Hide Communities
$('.row-5 .sidebar a').on('click', function(e) {
    e.preventDefault();
    var target = $($(this).attr('href'));
    $('.row-5 .mBar > div').removeClass('active');
    $(this).parent().addClass('active');
    $('.row-5 .content > div').addClass('animated fadeOutUp');
    $('.row-5 .content > div').removeClass('show animated fadeInUp fadeOutUp');
    target.addClass('animated fadeInUp show');
});


//Album Home



//Show-Hide Products