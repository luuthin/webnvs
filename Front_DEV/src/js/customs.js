var is_open_menu = false;
var data_product = [];


//comment
$(document).ready(function () {

    //toggle menu
    $('.toggle-nav').click(function () {
        toggle_menu()
    });

    // close menu when selected page
    $('.item-menu-click').click(function () {
        toggle_menu();
    });

    // scroll product
    document.getElementById("tab-content").addEventListener("mouseenter", mouseEnter);
    document.getElementById("tab-content").addEventListener("mouseleave", mouseLeave);

    document.getElementById("list-customer").addEventListener("mouseenter", mouseEnter);
    document.getElementById("list-customer").addEventListener("mouseleave", mouseLeave);

    document.getElementById("product-detail").addEventListener("mouseenter", mouseEnter);
    document.getElementById("product-detail").addEventListener("mouseleave", mouseLeave);

    // document.getElementById("page2").addEventListener("mouseenter", mouseEnter);
    // document.getElementById("page2").addEventListener("mouseleave", mouseLeave);

    // set fixed position
    var position = $('.post-content').height() - 35 + 'px';
    $('.page-content').css('top', position);

    setTimeout(function(){
        var position_nav = $('.post-content').height() - 45 + 'px';
        var nav = $('.page-menu .post-content .owl-nav');
        nav.css('top', position_nav);

        $('.products').css('text-align', 'start');
        $('.customer-col-left .owl-carousel .owl-nav button.owl-next').addClass('active');
        $('.page-menu .owl-carousel .owl-nav button.owl-next').addClass('active');

        // $('.product-detail-item').click(function () {
        //     $('#product-detail').show()
        //     $('#product-detail').addClass('animated is-animated fadeInLeft');
        //     $('#page-product').hide();
        // });

        var images = document.querySelectorAll(".product-image");
        new LazyLoad(images);

    }, 1000);
    

    //slide page customer
    $('.owl-carousel-customer').owlCarousel({
        items: 1,
        loop: true,
        autoplay: false,
        dots: true,
        nav: true
    }, 3000);

    //slide menu
    $('.post-content').owlCarousel({
        items: 1,
        loop: true,
        autoplay: false,
        dots: false,
        nav: true
    }, 3000);

    $(".products").imagesGrid({
        rowHeight: 260,
        margin: 0,
        imageSelector: '.product-image'
    });

    initCanvasTag();
    loadProductList();
    onHoverListCustomer();

});
var count = 0;
function toggle_menu() {
    is_open_menu = !is_open_menu;
    $('.nav-open').toggleClass('active');
    $('.nav-close').toggleClass('active');

    if ($('#header label span:nth-child(2)').css('color') == 'rgb(255, 255, 255)') {
        count = 1;
    }

    if (!is_open_menu) {
        $('.dropdown-nav').toggleClass('full');
        $('.home-theme').toggleClass('overflow-menu');
        $(".nav-close").css("color","#fff");
        if (count == 1) {
            $("#header label span:nth-child(2)").css("color","#fff");
            $(".logo_class").attr("src","images/icon/lohg 2-48.png");
        }
        count = 0
    }else{
        $(".nav-close").css("color","#000");
        if (count == 1) {
            $("#header label span:nth-child(2)").css("color","#000");
            $(".logo_class").attr("src","images/icon/logo-10.png");
        } 
    }
}

function initCanvasTag(){
    var option = {
        initial: [0.3,0],
        textColour : '#000',
        outlineColour : '#fff',
        outlineThickness : 0,
        maxSpeed : 0.05,
        depth : 0.75,
        wheelZoom: false,
        reverse: true
    };
    TagCanvas.Start('myCanvas', 'canvas-tags', option);
}

function mouseEnter() {
    $.fn.fullpage.setAllowScrolling(false);
}
  
function mouseLeave() {
    $.fn.fullpage.setAllowScrolling(true);
}

function loadProductList(){
    $.ajax({
        type: "get", 
        url: "data/product.json",
        datatype: "json",
        success: function (data) {
            data_product = data;
            for(var i = 0; i < data.length; i++){
                var tagID = '';

                switch (data[i].cat){
                    case 'bank':
                        tagID = '1a';
                        break;
                    case 'securities':
                        tagID = '2a';
                        break;
                    default:
                        tagID = '3a';
                        break;
                }
                addProductItemToList(tagID, data[i]);
            }
        },
        error: function (error) {
            console.log(error)
        }
    });
}

function addProductItemToList(tagId, data){
    var list = $('#' + tagId + '>ul')[0];

    var node = document.createElement("li");
    node.classList.add('thumbnail');
    var caption_tag = document.createElement("div");
    caption_tag.classList.add('caption');

    var a_tag = document.createElement("a");

    var p_tag = document.createElement("p"); 
    var txt_p = document.createTextNode(data.title);
    p_tag.appendChild(txt_p)
    p_tag.classList.add('title');
    var des_tag = document.createElement("p");
    des_tag.classList.add('desc');
    var txt_des = document.createTextNode(data.des);
    des_tag.appendChild(txt_des);

    var att = document.createAttribute("href");
    att.value = "javascript:void(0)";
    a_tag.setAttributeNode(att);
    var att = document.createAttribute("onclick");
    att.value = 'productDetailView('+ data.id +')';
    a_tag.setAttributeNode(att);

    var att_id = document.createAttribute("class");
    att_id.value = "product-detail-item";
    a_tag.setAttributeNode(att_id);     

    node.appendChild(caption_tag);
    caption_tag.appendChild(a_tag);
    a_tag.appendChild(p_tag);
    caption_tag.appendChild(des_tag);

    list.appendChild(node);
}

function onHoverListCustomer(){
    $( "#list-customer ul li" )
    .mouseenter(function() {
        $( "#list-customer ul li" ).removeClass('active')
        $(this).addClass('active')
    });
}

function productDetailView(product_id){
    openProductDetail();
    for(var i = 0; i< data_product.length; i++){
        if(product_id == data_product[i].id){
            $('#product_title').text(data_product[i].title);
            $('.product_content').html(data_product[i].content);
            break;
        }
    }
}

function openProductDetail(){
    $('#product-detail').show()
    $('#product-detail').addClass('animated is-animated fadeInLeft');
    $('#page-product').hide();
}
